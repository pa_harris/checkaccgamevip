﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace CheckAccGameVip
{
    public class ProxyAPI
    {
        static string GetSign(string license, string timeSpan, string secrect)
        {
            string sign = "";

            sign = Utils.MD5(license + timeSpan + secrect);

            return sign;
        }


        public static string[] GetProxy(string license, string timeSpan, string secrect, int count)
        {
            string[] proxy = null;
            string sign = GetSign(license, timeSpan, secrect);
            using (HttpClient client = new HttpClient())
            {
                var res = client.GetAsync($"https://api.ttproxy.com/v1/obtain?license={license}&time={timeSpan}&sign={sign}&cnt={count}").Result.Content.ReadAsStringAsync().Result;
                var resData = JsonConvert.DeserializeObject<TTProxyResponse>(res);

                proxy = resData.data.proxies;

                foreach (var item in proxy)
                {
                    string ip = client.GetAsync("https://api64.ipify.org").Result.Content.ReadAsStringAsync().Result;

                    var aa = client.GetAsync($"https://api.ttproxy.com/v1/whitelist/add?license={license}&time={timeSpan}&sign={sign}&ip={ip}").Result.Content.ReadAsStringAsync().Result;
                }
            }

            return proxy;
        }
    }


    public class TTProxyResponse
    {
        public int code { get; set; }
        public string error { get; set; }
        public string _id { get; set; }
        public Data data { get; set; }
    }

    public class Data
    {
        public int todayObtain { get; set; }
        public object ipLeft { get; set; }
        public long trafficLeft { get; set; }
        public string[] proxies { get; set; }
    }
}
