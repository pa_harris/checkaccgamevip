﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CheckAccGameVip
{
    public class CheckAccModel
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string Md5Password { get; set; }
        public string OTP { get; set; } = "";
        public string SigningKey { get; set; }
    }

}
