﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CheckAccGameVip
{
    public class AccsOutput
    {
        public string id { get; set; }
        public string pass { get; set; }
        public string tennv { get; set; }
        public string vippoint { get; set; }
        public string sdt { get; set; }
        public string telesafe { get; set; }
        public string timechanged { get; set; }
        public string status { get; set; } = "";
        public int proxyId { get; set; }
        public string goldBalance { get; set; }
        public string coinBalance { get; set; }

        public override string ToString()
        {
            return $"{id}|{pass}|{tennv}|{vippoint}|{sdt}|{telesafe}|{timechanged}|{status}|{proxyId}|{goldBalance}|{coinBalance}";
        }
    }
}
