﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CheckAccGameVip
{
    public class GameGate
    {
        public string Name { get; set; }
        public string LoginApi { get; set; }
        public string GetStockApi { get; set; }
        public string Schema { get; set; }
    }

}
