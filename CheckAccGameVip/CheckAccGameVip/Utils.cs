﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace CheckAccGameVip
{
	internal class Utils
	{
		// Token: 0x060000B3 RID: 179 RVA: 0x00006710 File Offset: 0x00004910
		public static string Base64Decode(string base64EncodedData)
		{
			byte[] bytes = Convert.FromBase64String(base64EncodedData);
			return Encoding.UTF8.GetString(bytes);
		}

		// Token: 0x060000B4 RID: 180 RVA: 0x00006734 File Offset: 0x00004934
		public static string MD5(string input)
		{
			string result;
			using (MD5 md = System.Security.Cryptography.MD5.Create())
			{
				byte[] bytes = Encoding.ASCII.GetBytes(input);
				byte[] array = md.ComputeHash(bytes);
				StringBuilder stringBuilder = new StringBuilder();
				for (int i = 0; i < array.Length; i++)
				{
					stringBuilder.Append(array[i].ToString("x2"));
				}
				result = stringBuilder.ToString();
			}
			return result;
		}

		public static String GetTimestamp(DateTime value, int digit = 12)
		{
			TimeSpan t = (DateTime.UtcNow - new DateTime(1970, 1, 1));
			long timestamp = (long)t.TotalMilliseconds;
			return new string(timestamp.ToString().Take(digit).ToArray());

			return value.ToString("yyyyMMddHHmmssffff");
		}

		static string GetEncodeStep1(string userName, string password)
		{
			var data = Utils.MD5($"{userName}.{password};1,1-{DateTime.Now.ToString("ddMMyyyy")}");
			return data;
		}
		public static string GetSigningKey(string userName, string password)
		{
			var data = GetEncodeStep1(userName, password);
			return data.Substring(0, data.Length - 1);
		}
	}
}
