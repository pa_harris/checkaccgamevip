﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CheckAccGameVip
{
    public class CheckGamVipResultModel
    {
        public int c { get; set; }
        public string m { get; set; }
        public string[] p { get; set; }
        public D d { get; set; }
    }

    public class D
    {
        public int accountID { get; set; }
        public string username { get; set; }
        public string nickname { get; set; }
        public string avatar { get; set; }
        public string email { get; set; }
        public string passport { get; set; }
        public string mobile { get; set; }
        public string teleSafe { get; set; }
        public int confirmStatus { get; set; }
        public int securityStatus { get; set; }
        public DateTime lastChange { get; set; }
        public string sessionToken { get; set; }
        public string securityToken { get; set; }
        public int sourceID { get; set; }
        public string clientIP { get; set; }
        public int portalID { get; set; }
        public int vipLevel { get; set; }
        public int vipPoint { get; set; }
        public int goldBalance { get; set; }
        public int coinBalance { get; set; }
        public int currencyID { get; set; }
        public int vpMin { get; set; }
        public int vpMax { get; set; }
        public DateTime activatedTime { get; set; }
        public int verifyTypeID { get; set; }
        public object originNickname { get; set; }
        public int redirectType { get; set; }
    }
}
